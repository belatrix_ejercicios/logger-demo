package com.kenyo.logger;

import java.text.DateFormat;
import java.util.Date;

import com.kenyo.logger.base.ILogger;
import com.kenyo.logger.config.Configuration;
import com.kenyo.logger.util.MessageException;
import com.kenyo.logger.util.MessageType;
import com.kenyo.logger.util.admin.DataBaseAdmin;

public class LoggerDataBase implements ILogger{

	private DataBaseAdmin admin;
	
	private String template = "%1$s %2$s %3s";
	
	public LoggerDataBase(Configuration configuration) {
		this.admin = DataBaseAdmin.getInstance(configuration);
	}

	public void writeError(String message) {
		if("".equals(message))
			throw new MessageException("El error debería ser indicado");
		String _message = String.format(template, "error", DateFormat.getDateInstance(DateFormat.LONG).format(new Date()), message);
		this.admin.insertMessage(MessageType.ERROR.id, _message);
	}

	public void writeWarning(String message) {
		if("".equals(message))
			throw new MessageException("La advertencia debería ser indicada");
		String _message = String.format(template, "warning", DateFormat.getDateInstance(DateFormat.LONG).format(new Date()), message);
		this.admin.insertMessage(MessageType.WARNING.id, _message);
		
	}

	public void writeMessage(String message) {
		if("".equals(message))
			throw new MessageException("El mensaje debería ser indicado");
		String _message = String.format(template, "message", DateFormat.getDateInstance(DateFormat.LONG).format(new Date()), message);
		this.admin.insertMessage(MessageType.MESSAGE.id, _message);
		
	}
}
