package com.kenyo.logger.base;

import java.util.logging.Logger;


public interface ILogger {

	public static final Logger LOGGER = Logger.getLogger("ILogger");
	
	void writeError(String message);
	void writeWarning(String message);
	void writeMessage(String message);
}
