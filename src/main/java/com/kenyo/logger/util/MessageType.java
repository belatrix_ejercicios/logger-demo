package com.kenyo.logger.util;

public enum MessageType {

	ERROR(1, "ERROR"),
	WARNING(2, "WARNING"),
	MESSAGE(3, "MESSAGE");
	
	public final int id;
	public final String type;
	
	MessageType(int id, String type){
		this.id = id;
		this.type = type;
	}
}
