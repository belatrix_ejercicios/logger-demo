package com.kenyo.logger;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.kenyo.logger.base.ILogger;
import com.kenyo.logger.base.LoggerFactory;
import com.kenyo.logger.util.LoggerType;

public class LoggerConsoleTest {
	
	@Test
	public void factoryType() {
		ILogger logger = LoggerFactory.getLogger(LoggerType.CONSOLE.type);
		assertEquals(true, logger instanceof LoggerConsole);
	}
	
	@Test
	public void writeMessage() {
		ILogger logger = LoggerFactory.getLogger(LoggerType.CONSOLE.type);
		logger.writeError("Test - mensaje de error");
		assertEquals(true, logger instanceof LoggerConsole);
	}
	
	@Test
	public void writeMessages() {
		ILogger logger = LoggerFactory.getLogger(LoggerType.CONSOLE.type);
		logger.writeMessage("Test - mensaje de info");
		logger.writeWarning("Test - mensaje de arvertencia");
		logger.writeError("Test - mensaje de error");
		assertEquals(true, logger instanceof LoggerConsole);
	}

}
