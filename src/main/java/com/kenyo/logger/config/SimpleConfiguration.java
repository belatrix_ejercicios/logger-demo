package com.kenyo.logger.config;

public class SimpleConfiguration extends Configuration {

	@Override
	public String getProperty(String property) {
		String _property = null;
		if (property.equals("logger.file.path")) {
			_property = "./log_file.txt";
		} else if (property.equals("logger.bd.user")) {
			_property = "kenyo";
		} else if (property.equals("logger.bd.password")) {
			_property = "kenyo";
		} else if (property.equals("logger.bd.jdbcDriver")) {
			_property = "org.h2.Driver";
		} else if (property.equals("logger.bd.url")) {
			_property = "jdbc:h2:./loggerDB";
		}
		
		return _property;
	}

}
