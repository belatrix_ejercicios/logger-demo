package com.kenyo.logger.util.admin;

import java.io.Serializable;
import java.util.logging.ConsoleHandler;

import com.kenyo.logger.config.Configuration;

public class ConsoleAdmin implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9129141563385308649L;
	
	private static ConsoleAdmin instance = null;
	private Configuration configuration;
	
	private ConsoleAdmin(Configuration configuration) {
		this.configuration = configuration;
	}
	
	public static synchronized ConsoleAdmin getInstance(Configuration configuration) {
		if(instance == null)
			instance = new ConsoleAdmin(configuration);
		return instance;
	}
	
	public ConsoleHandler getConsoleHandler() {
		return new ConsoleHandler();
	}

}
