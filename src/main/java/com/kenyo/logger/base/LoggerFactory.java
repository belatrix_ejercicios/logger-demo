package com.kenyo.logger.base;

import com.kenyo.logger.LoggerConsole;
import com.kenyo.logger.LoggerDataBase;
import com.kenyo.logger.LoggerFile;
import com.kenyo.logger.config.Configuration;
import com.kenyo.logger.config.SimpleConfiguration;
import com.kenyo.logger.util.LoggerType;

public class LoggerFactory {
	
	private LoggerFactory() {
		super();
	}
	
	public static ILogger getLogger(String type) {
		if(type == null) {
			throw new RuntimeException("No se especificó tipo");
		}
		if(LoggerType.CONSOLE.type.equalsIgnoreCase(type)) {
			return new LoggerConsole(new SimpleConfiguration());
		} else if(LoggerType.DATABASE.type.equalsIgnoreCase(type)) {
			return new LoggerDataBase(new SimpleConfiguration());
		} else if(LoggerType.FILE.type.equalsIgnoreCase(type)) {
			return new LoggerFile(new SimpleConfiguration());
		} else {
			throw new RuntimeException("Tipo no válido");
		}
	}
	
	
	public static ILogger getLogger(String type, Configuration configuration) {
		if(type == null) {
			throw new RuntimeException("No se especificó tipo");
		}
		if(LoggerType.CONSOLE.type.equalsIgnoreCase(type)) {
			return new LoggerConsole(configuration);
		} else if(LoggerType.DATABASE.type.equalsIgnoreCase(type)) {
			return new LoggerDataBase(configuration);
		} else if(LoggerType.FILE.type.equalsIgnoreCase(type)) {
			return new LoggerFile(configuration);
		} else {
			throw new RuntimeException("Tipo no válido");
		}
	}

}
