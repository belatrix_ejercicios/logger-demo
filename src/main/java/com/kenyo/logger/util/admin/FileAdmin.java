package com.kenyo.logger.util.admin;

import java.io.File;
import java.io.Serializable;
import java.util.logging.FileHandler;

import com.kenyo.logger.config.Configuration;

public class FileAdmin  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6988140955377002393L;
	
	private static FileAdmin instance = null;
	
	private Configuration configuration;
	
	private FileAdmin(Configuration configuration) {
		this.configuration = configuration;
	}
	
	public static synchronized FileAdmin getInstance(Configuration configuration) {
		if(instance == null)
			return new FileAdmin(configuration);
		return instance;
	}
	
	public File getLogFile() {
		File logFile = new File(this.configuration.getProperty("logger.file.path"));
		if(!logFile.exists()) {
			try {
				logFile.createNewFile();
			} catch (Exception e) {
				throw new RuntimeException("Create new Log file filed", e);
			}
		}
		return logFile;
	}
	
	public FileHandler getFileHandler() {
		try {
			getLogFile();
			return new FileHandler(this.configuration.getProperty("logger.file.path"));
		} catch (Exception e) {
			throw new RuntimeException("handler Log file filed", e);
		}
	}

	
}
