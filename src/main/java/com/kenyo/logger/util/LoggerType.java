package com.kenyo.logger.util;

public enum LoggerType {

	FILE("FILE"),
	DATABASE("DATABASE"),
	CONSOLE("CONSOLE");
	
	public final String type;
	
	LoggerType(String type){
		this.type = type;
	}
}
