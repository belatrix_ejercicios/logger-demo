package com.kenyo.logger.util.admin;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import com.kenyo.logger.config.Configuration;

public class DataBaseAdmin implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6916059531115911728L;
	
	private static DataBaseAdmin instance = null;
	
	private Configuration configuration;
	
	private DataBaseAdmin(Configuration configuration) {
		this.configuration = configuration;
	}
	
	public static synchronized DataBaseAdmin getInstance(Configuration configuration) {
		if(instance == null)
			return new DataBaseAdmin(configuration);
		return instance;
	}
	
	public Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName(this.configuration.getProperty("logger.bd.jdbcDriver"));
			String user = this.configuration.getProperty("logger.bd.user");
			String password = this.configuration.getProperty("logger.bd.password");
			String dbURL = this.configuration.getProperty("logger.bd.url");
			connection = DriverManager.getConnection(dbURL, user, password);
		} catch (Exception e) {
			throw new RuntimeException("Connection DB failed", e);
		}
		return connection;
	}
	
	public Statement getStatment(Connection connection) {
		try {
			return connection.createStatement();
		} catch (SQLException e) {
			throw new RuntimeException("Get Statment Connection failed", e);
		}
	}
	
	public void createTable() {
		Connection connection = null;
		Statement statement = null;
		try {
			connection = getConnection();
			statement = getStatment(connection);
			statement.executeUpdate("CREATE TABLE IF NOT EXISTS LOG_DATA(type int, message varchar(255))");
		} catch (Exception e) {
			throw new RuntimeException("Create log table failed", e);
		}
	}
	
	public void insertMessage(int typeMessage, String message) {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection();
			String insertQuery = "INSERT INTO LOG_DATA(type, message) VALUES(?, ?)";
			statement = connection.prepareStatement(insertQuery);
			statement.setInt(1, typeMessage);
			statement.setString(2, message);
			statement.executeUpdate();
		} catch (Exception e) {
			throw new RuntimeException("Insert message BD failed.", e);
		}
	}

}
