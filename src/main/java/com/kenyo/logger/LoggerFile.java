package com.kenyo.logger;

import java.util.logging.Level;

import com.kenyo.logger.base.ILogger;
import com.kenyo.logger.config.Configuration;
import com.kenyo.logger.util.MessageException;
import com.kenyo.logger.util.admin.FileAdmin;

public class LoggerFile implements ILogger {
	
	private FileAdmin admin;
	
	
	public LoggerFile(Configuration configuration) {
		this.admin = FileAdmin.getInstance(configuration);
		LOGGER.addHandler(this.admin.getFileHandler());
	}
	


	public void writeError(String message) {
		if("".equals(message))
			throw new MessageException("El error debería ser indicado");
		LOGGER.log(Level.SEVERE, message);
	}

	public void writeWarning(String message) {
		if("".equals(message))
			throw new MessageException("La arvertencia debería ser indicada");
		LOGGER.log(Level.WARNING, message);
	}

	public void writeMessage(String message) {
		if("".equals(message))
			throw new MessageException("El mensaje debería ser indicado");
		LOGGER.log(Level.INFO, message);
	}

}
