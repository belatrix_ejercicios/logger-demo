package com.kenyo.logger.util;

public class MessageException  extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6219384114941291348L;
	
	public MessageException(String message) {
		super(message);
	}

}
