package com.kenyo.logger;

import java.util.logging.Level;

import com.kenyo.logger.base.ILogger;
import com.kenyo.logger.config.Configuration;
import com.kenyo.logger.util.MessageException;
import com.kenyo.logger.util.admin.ConsoleAdmin;

public class LoggerConsole implements ILogger {
	
	private ConsoleAdmin admin;
	
	public LoggerConsole(Configuration configuration) {
		this.admin = ConsoleAdmin.getInstance(configuration);
		LOGGER.addHandler(this.admin.getConsoleHandler());
	}

	public void writeError(String message) {
		if("".equals(message))
			throw new MessageException("El mensaje debería ser indicado");
		LOGGER.log(Level.SEVERE, message);
	}

	public void writeWarning(String message) {
		if("".equals(message))
			throw new MessageException("El mensaje debería ser indicado");
		LOGGER.log(Level.WARNING, message);
	}

	public void writeMessage(String message) {
		if("".equals(message))
			throw new MessageException("El mensaje debería ser indicado");
		LOGGER.log(Level.INFO, message);
	}

}
