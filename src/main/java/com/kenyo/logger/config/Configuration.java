package com.kenyo.logger.config;

public abstract class Configuration {

	public abstract String getProperty(String property);
}
