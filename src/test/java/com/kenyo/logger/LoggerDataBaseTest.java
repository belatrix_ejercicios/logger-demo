package com.kenyo.logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.kenyo.logger.base.ILogger;
import com.kenyo.logger.base.LoggerFactory;
import com.kenyo.logger.config.SimpleConfiguration;
import com.kenyo.logger.util.LoggerType;
import com.kenyo.logger.util.MessageType;
import com.kenyo.logger.util.admin.DataBaseAdmin;

public class LoggerDataBaseTest {
	
	@Test
	public void factoryType() {
		ILogger logger = LoggerFactory.getLogger(LoggerType.DATABASE.type);
		assertEquals(true, logger instanceof LoggerDataBase);
	}
	
	@Test
	public void testConecction() {
		DataBaseAdmin admin = DataBaseAdmin.getInstance(new SimpleConfiguration());
		try {
			assertTrue(admin.getConnection().isValid(0));
		} catch (Exception e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void writeMessage() {
		try {
			DataBaseAdmin admin = DataBaseAdmin.getInstance(new SimpleConfiguration());
			admin.createTable();
			admin.insertMessage(MessageType.ERROR.id, "Test insert error");
			assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}
	
	@Test
	public void writeMessages() {
		try {
			ILogger logger = LoggerFactory.getLogger(LoggerType.DATABASE.type);
			logger.writeError("Test - Mensaje ERROR");
			logger.writeMessage("Test - Mensaje OK");
			logger.writeWarning("Test - Mensaje ADVERTENCIA");
			assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}

}
